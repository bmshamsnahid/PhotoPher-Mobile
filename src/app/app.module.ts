import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {AlbumPage} from "../pages/album/album";
import {BuylistPage} from "../pages/buylist/buylist";
import {CategoryPage} from "../pages/category/category";
import {HomePage} from "../pages/home/home";
import {PhotoPage} from "../pages/photo/photo";
import {WishlistPage} from "../pages/wishlist/wishlist";
import {IntroPage} from "../pages/intro/intro";
import {LoginPage} from "../pages/login/login";
import {SignupPage} from "../pages/signup/signup";
import {SettingsPage} from "../pages/settings/settings";
import {ViewProfilePage} from "../pages/view-profile/view-profile";
import {EditProfilePageModule} from "../pages/edit-profile/edit-profile.module";
import {AddAlbumPageModule} from "../pages/add-album/add-album.module";
import {PhotographerAlbumListPageModule} from "../pages/photographer-album-list/photographer-album-list.module";
import {AddPhotosPageModule} from "../pages/add-photos/add-photos.module";
import {PhotographerPhotosListPageModule} from "../pages/photographer-photos-list/photographer-photos-list.module";
import {AllMessagePageModule} from "../pages/all-message/all-message.module";
import {UnseenMessagePageModule} from "../pages/unseen-message/unseen-message.module";
import {WishlistPageModule} from "../pages/wishlist/wishlist.module";
import {BuylistPageModule} from "../pages/buylist/buylist.module";
import {EditAlbumPageModule} from "../pages/edit-album/edit-album.module";
import {SinglePhotoViewerPageModule} from "../pages/single-photo-viewer/single-photo-viewer.module";
import {ViewProfilePageModule} from "../pages/view-profile/view-profile.module";
import {SinglePhotoEditorPageModule} from "../pages/single-photo-editor/single-photo-editor.module";
import {AlbumPhotoPageModule} from "../pages/album-photo/album-photo.module";
import {SinglePhotographerProfilePage} from "../pages/single-photographer-profile/single-photographer-profile";
import {SinglePhotographerProfilePageModule} from "../pages/single-photographer-profile/single-photographer-profile.module";
import {CategorizedAlbumPageModule} from "../pages/categorized-album/categorized-album.module";
import {PhotographerPublicAlbumPageModule} from "../pages/photographer-public-album/photographer-public-album.module";
import {PhotographerPublicProfilePage} from "../pages/photographer-public-profile/photographer-public-profile";
import {PhotographerPublicProfilePageModule} from "../pages/photographer-public-profile/photographer-public-profile.module";
import { SigninProvider } from '../providers/signin/signin';
import {IonicStorageModule} from "@ionic/storage";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from '@angular/common/http';
import {HttpModule} from "@angular/http";
import { SignupProvider } from '../providers/signup/signup';
import { PhotographerProvider } from '../providers/photographer/photographer';
import { CallNumber } from '@ionic-native/call-number';
import { SMS } from '@ionic-native/sms';
import { AlbumProvider } from '../providers/album/album';
import { PhotoProvider } from '../providers/photo/photo';
import { CategoryProvider } from '../providers/category/category';
import {AddCategoryPageModule} from "../pages/add-category/add-category.module";
import {FileUploadModule, FileSelectDirective} from "ng2-file-upload";
import {SelectAlbumPageModule} from "../pages/select-album/select-album.module";
import {PublicSinglePhotoViewerPageModule} from "../pages/public-single-photo-viewer/public-single-photo-viewer.module";
import { StripeProvider } from '../providers/stripe/stripe';
import { Stripe } from '@ionic-native/stripe';
import { WishlistsProvider } from '../providers/wishlists/wishlists';


@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    AlbumPage,
    CategoryPage,
    HomePage,
    IntroPage,
    LoginPage,
    PhotoPage,
    SignupPage,
    SettingsPage,
  ],
  imports: [
    BrowserModule,
    EditProfilePageModule,
    AddAlbumPageModule,
    PhotographerAlbumListPageModule,
    AddPhotosPageModule,
    PhotographerPhotosListPageModule,
    WishlistPageModule,
    BuylistPageModule,
    AllMessagePageModule,
    UnseenMessagePageModule,
    EditAlbumPageModule,
    SinglePhotoViewerPageModule,
    SinglePhotoEditorPageModule,
    AlbumPhotoPageModule,
    SinglePhotographerProfilePageModule,
    CategorizedAlbumPageModule,
    ViewProfilePageModule,
    PhotographerPublicAlbumPageModule,
    PhotographerPublicProfilePageModule,
    AddCategoryPageModule,
    PublicSinglePhotoViewerPageModule,
    FileUploadModule,
    SelectAlbumPageModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule,

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    AlbumPage,
    CategoryPage,
    HomePage,
    IntroPage,
    LoginPage,
    PhotoPage,
    SignupPage,
    SettingsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SigninProvider,
    SignupProvider,
    PhotographerProvider,
    CallNumber,
    SMS,
    AlbumProvider,
    PhotoProvider,
    CategoryProvider,
    StripeProvider,
    WishlistsProvider
  ]
})
export class AppModule {}
