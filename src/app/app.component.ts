import { Component } from '@angular/core';
import {Platform, ToastController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {IntroPage} from "../pages/intro/intro";
import {TabsPage} from "../pages/tabs/tabs";
import {EditAlbumPage} from "../pages/edit-album/edit-album";
import {AllMessagePage} from "../pages/all-message/all-message";
import {UnseenMessagePage} from "../pages/unseen-message/unseen-message";
import {WishlistPage} from "../pages/wishlist/wishlist";
import {BuylistPage} from "../pages/buylist/buylist";
import {PhotoPage} from "../pages/photo/photo";
import {AlbumPage} from "../pages/album/album";
import {CategoryPage} from "../pages/category/category";
import {LoginPage} from "../pages/login/login";
import {SigninProvider} from "../providers/signin/signin";
import {User} from "../model/user";
import {HomePage} from "../pages/home/home";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  // rootPage: any = IntroPage;
  // rootPage: any = LoginPage;
  // rootPage: any = BuylistPage;
  rootPage: any;
  currentUser: any;

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private signInProvider: SigninProvider,
              private toastCtrl: ToastController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.initiatePage();
  }

  initiatePage() {
    try {
      if (this.signInProvider.isPhotographerLoggedIn()) {
        this.rootPage = TabsPage;
        let user: User = new User();
        user.email = localStorage.getItem('email');
        user.password = localStorage.getItem('password');
        this.signInProvider.userSignIn(user).subscribe((response) => {
          if (response.success) {
            this.presentToast('Your entry activity updated.')
          } else {
            this.presentToast('Please login again.');
            this.rootPage = LoginPage;
          }
        });
      } else {
        this.rootPage = IntroPage;
      }
    } catch (e) {
      console.log(e);
      this.rootPage = IntroPage;
    }
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
}
