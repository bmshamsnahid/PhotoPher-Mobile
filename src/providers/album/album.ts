import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MyConfig } from "../../model/my-config";
import {User} from "../../model/user";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {ToastController} from 'ionic-angular';
import {Album} from "../../model/album";

/*
  Generated class for the AlbumProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AlbumProvider {

  headers: Headers;
  myConfig: MyConfig;

  currentUserObj;
  currentUser: User;
  token;

  constructor(public http: Http,
              public toastCtrl: ToastController) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.currentUser = this.currentUserObj.currentUser;
      this.token = this.currentUserObj.token;
    }

    this.myConfig = new MyConfig();
    this.headers = new Headers();
    this.headers.append("Content-Type", "application/json");
    this.headers.append("clientid", this.myConfig.clientId);
    this.headers.append("clientsecret", this.myConfig.clientSecret);
    this.headers.append("Authorization", this.token);
  }

  getPhotographerAllAlbums() {
    const options = new RequestOptions({ headers: this.headers});
    return this.http.get(`${this.myConfig.baseURL}/api/album/photographerAllAlbum/${this.currentUser.photographerId}`, options)
      .map((response: Response) => {
        console.log(response);
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
        }
      });
  }

  getAllPublicAndPrivateAlbumsInfo() {
    console.log('Getting all the album lsits provider section.')
    const options = new RequestOptions({ headers: this.headers});
    return this.http.get(`${this.myConfig.baseURL}/api/album/photographer/${this.currentUser.photographerId}`, options)
      .map((response: Response) => {
        console.log('Album Provider Response');
        console.log(response);
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
        }
      });
  }

  getPhotographerPublicAlbums(photographerId: string) {
    const options = new RequestOptions({ headers: this.headers});
    return this.http.get(`${this.myConfig.baseURL}/api/album/photographersPublicAlbumsInfo/${photographerId}`, options)
      .map((response: Response) => {
        console.log(response);
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
        }
      });
  }

  getAllPublicAlbumsInfo() {
    const options = new RequestOptions({ headers: this.headers});
    return this.http.get(`${this.myConfig.baseURL}/api/album/publicAlbumsInfo`, options)
      .map((response: Response) => {
        console.log(response);
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
        }
      });
  }

  createAlbum(album: Album) {
    const options = new RequestOptions({ headers: this.headers});
    return this.http.post(`${this.myConfig.baseURL}/api/album`, JSON.stringify(album), options)
      .map((response: Response) => {
        console.log(response);
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
        }
      });
  }

  updateAlbum(album: Album) {
    const options = new RequestOptions({ headers: this.headers});
    return this.http.patch(`${this.myConfig.baseURL}/api/album/${album._id}`, JSON.stringify(album), options)
      .map((response: Response) => {
        console.log(response);
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
        }
      });
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
}
