import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MyConfig } from "../../model/my-config";
import {User} from "../../model/user";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {ToastController} from 'ionic-angular';
import {Photographer} from "../../model/photographer";

/*
  Generated class for the PhotographerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PhotographerProvider {

  headers: Headers;
  myConfig: MyConfig;

  currentUserObj;
  currentUser;
  token;

  constructor(public http: Http,
              public toastCtrl: ToastController) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.currentUser = this.currentUserObj.currentUser;
      console.log(this.currentUser);
      this.token = this.currentUserObj.token;
    }

    this.myConfig = new MyConfig();
    this.headers = new Headers();
    this.headers.append("Content-Type", "application/json");
    this.headers.append("clientid", this.myConfig.clientId);
    this.headers.append("clientsecret", this.myConfig.clientSecret);
    this.headers.append("Authorization", this.token);
  }

  getAllPhotographers() {
    const options = new RequestOptions({ headers: this.headers});
    return this.http.get(`${this.myConfig.baseURL}/api/photographer`, options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
        }
      });
  }

  getPhotographerPrivateInformation(photographerId: string) {
    const options = new RequestOptions({ headers: this.headers});
    return this.http.get(`${this.myConfig.baseURL}/api/photographer/photographerPrivateInformation/${photographerId}`, options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
        }
      });
  }

  getAPhotographer(photographerId: string) {
    const options = new RequestOptions({ headers: this.headers});
    return this.http.get(`${this.myConfig.baseURL}/api/photographer/${photographerId}`, options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
        }
      });
  }

  updatePhotographer(photographer: Photographer, photographerId: string) {
    const options = new RequestOptions({ headers: this.headers});
    return this.http.patch(`${this.myConfig.baseURL}/api/photographer/${photographerId}`, JSON.stringify(photographer), options)
      .map((response: Response) => {
        console.log(response);
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
          console.log(response);
        }
      });
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
