import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MyConfig } from "../../model/my-config";
import {User} from "../../model/user";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {ToastController} from 'ionic-angular';
import {Photographer} from "../../model/photographer";
import {Category} from "../../model/category";

@Injectable()
export class CategoryProvider {

  headers: Headers;
  myConfig: MyConfig;

  currentUserObj;
  currentUser;
  token;

  constructor(public http: Http,
              public toastCtrl: ToastController) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.currentUser = this.currentUserObj.currentUser;
      this.token = this.currentUserObj.token;
    }

    this.myConfig = new MyConfig();
    this.headers = new Headers();
    this.headers.append("Content-Type", "application/json");
    this.headers.append("clientid", this.myConfig.clientId);
    this.headers.append("clientsecret", this.myConfig.clientSecret);
    this.headers.append("Authorization", this.token);
  }

  getAllCategory() {
    const options = new RequestOptions({ headers: this.headers});
    return this.http.get(`${this.myConfig.baseURL}/api/category/`, options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
        }
      });
  }

  createCategory(category: Category) {
    const options = new RequestOptions({ headers: this.headers});
    return this.http.post(`${this.myConfig.baseURL}/api/category/`, JSON.stringify(category), options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
        }
      });
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
