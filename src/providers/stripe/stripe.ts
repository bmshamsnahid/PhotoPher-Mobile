import {Injectable} from "@angular/core";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {MyConfig} from "../../model/my-config";
/*
  Generated class for the StripeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StripeProvider {

  headers: Headers;
  myConfig: MyConfig;
  currentUserObj;
  currentUser;
  token;

  constructor(private http: Http) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.currentUser = this.currentUserObj.currentUser;
      console.log(this.currentUser);
      this.token = this.currentUserObj.token;
    }

    this.myConfig = new MyConfig();
    this.headers = new Headers();
    this.headers.append("Content-Type", "application/json");
    this.headers.append("clientid", this.myConfig.clientId);
    this.headers.append("clientsecret", this.myConfig.clientSecret);
    this.headers.append("Authorization", this.token);
  }

  confirmPayment(stripeEmail, stripeToken) {
    const options = new RequestOptions({ headers: this.headers });

    let body = {
      stripeEmail: stripeEmail,
      stripeToken: stripeToken
    };

    return this.http.post(`${this.myConfig.baseURL}/api/stripe/charge`, JSON.stringify(body), options)
      .map((response: Response) => response)
      .catch(this.handleError);
  }

  private handleError(error?: Response) {
    if (error) {
      console.log(error);
      return Observable.throw(error.json().error || 'Server Error');
    } else {
      console.log('Unknown err');
    }
  }

}
