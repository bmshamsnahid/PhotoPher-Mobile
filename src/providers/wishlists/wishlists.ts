import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions, Response} from "@angular/http";
import {MyConfig} from "../../model/my-config";
import {ToastController} from "ionic-angular";
import {User} from "../../model/user";
import {Photo} from "../../model/photo";

/*
  Generated class for the WishlistsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class WishlistsProvider {

  headers: Headers;
  myConfig: MyConfig;

  currentUserObj;
  user: User;
  photographerId: string;
  token;


  constructor(public http: Http,
              public toastCtrl: ToastController) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.user = this.currentUserObj.currentUser;
      this.photographerId = this.user.photographerId;
      this.token = this.currentUserObj.token;
    }

    this.myConfig = new MyConfig();
    this.headers = new Headers();
    this.headers.append("Content-Type", "application/json");
    this.headers.append("clientid", this.myConfig.clientId);
    this.headers.append("clientsecret", this.myConfig.clientSecret);
    this.headers.append("Authorization", this.token);
  }

  getWishListsPhoto() {
    const options = new RequestOptions({ headers: this.headers});
    return this.http.get(`${this.myConfig.baseURL}/api/wishList/${this.photographerId}`, options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
        }
      });
  }

  removePhotoFromWishLists(photo: Photo) {
    const options = new RequestOptions({ headers: this.headers});
    return this.http.get(`${this.myConfig.baseURL}/api/wishList/${this.photographerId}/${photo._id}`, options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
        }
      });
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
