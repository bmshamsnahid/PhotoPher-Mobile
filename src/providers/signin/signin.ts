import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MyConfig } from "../../model/my-config";
import {User} from "../../model/user";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {ToastController} from 'ionic-angular';

/*
  Generated class for the SigninProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SigninProvider {

  headers: Headers;
  myConfig: MyConfig;
  currentUser: any;

  constructor(public http: Http,
              public toastCtrl: ToastController) {
    // console.log('Hello SigninProvider Provider');
    this.myConfig = new MyConfig();
    this.headers = new Headers();
    this.headers.append("Content-Type", "application/json");
    this.headers.append("clientid", this.myConfig.clientId);
    this.headers.append("clientsecret", this.myConfig.clientSecret);
  }

  isPhotographerLoggedIn(): boolean {
    try {
      let currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
      let currentUser = currentUserObj.currentUser;

      if (currentUser) {
        this.currentUser = currentUser;
        if (this.currentUser.isPhotographer == true) {
          return true;
        } else {
          // this.toastrService.warning('Logged in as a photographerProfile to get the resources.');
          return false;
        }
      }
    } catch (e) {
      return false;
    }
    return false;
  }

  userSignIn(user) {
    const options = new RequestOptions({ headers: this.headers});

    return this.http.post(`${this.myConfig.baseURL}/api/auth/login`, JSON.stringify(user), options)
      .map((response: Response) => {
        if (response.json().success) {
          localStorage.setItem('email', user.email);
          localStorage.setItem('password', user.password);
          let userObj: any = {};
          userObj.currentUser = response.json().data;
          userObj.token = response.json().token;
          localStorage.setItem('currentUserObj', JSON.stringify(userObj));
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message);
        } else {
          this.presentToast('Fatal Server Error');
        }
      });
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
