export class Message {
  constructor() {}
  _id: string;
  title: string;
  description: string;
  userId: string;
  isRead: boolean;
  date: Date;
  senderName: string;
  senderPhoneNumber: string;
}
