export class Photographer {
  constructor() {}
  _id: string;
  name: string;
  country: string;
  city: string;
  description: string;
  contactNumber: string;
  email: string;
  role: string;
  password: string;
  createdDate: Date;
  upVotes: string[];
  wishlists: string[];
  buyLists: string[];
  isPremium: boolean;
  netBalance: number;
}
