import {Album} from "./album";
import {Category} from "./category";
import {Photo} from "./photo";
import {Photographer} from "./photographer";

export class AlbumInfo {
  constructor() {};
  album: Album;
  albumCategory: Category;
  photos: Photo[];
  owner: Photographer;
}
