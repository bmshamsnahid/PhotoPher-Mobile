export class User {
  _id: string;
  email: string;
  password: string;
  createdDate: Date;
  role: string;
  isAdmin: boolean;
  isSuperAdmin: boolean;
  isPhotographer: boolean;
  isRecruiter: boolean;
  photographerId: string;
}
