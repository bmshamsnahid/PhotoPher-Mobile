import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {Photographer} from "../../model/photographer";
import {PhotographerProvider} from "../../providers/photographer/photographer";
import {SettingsPage} from "../settings/settings";
import {User} from "../../model/user";

@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

  currentUserObj;
  user: User;
  photographer: Photographer;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public toastCtrl: ToastController,
              private photographerProvider: PhotographerProvider) {
    this.photographer = new Photographer();
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.user = this.currentUserObj.currentUser;
      this.photographerProvider.getAPhotographer(this.user.photographerId)
        .subscribe((response) => {
          if (response.success) {
            this.photographer = response.data;
          }
        });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProfilePage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onClickUpdateProfile() {
    if (!this.photographer.name) {
      this.presentToast('Invalid or incomplete name.');
    } else if (!this.photographer.country) {
      this.presentToast('Invalid or incomplete country.');
    } else if (!this.photographer.city) {
      this.presentToast('Invalid or incomplete city.');
    } else if (!this.photographer.contactNumber) {
      this.presentToast('Invalid or incomplete contact number.');
    } else {
      if (typeof this.photographer.isPremium == 'undefined') {
        this.photographer.isPremium = false;
      }
      if (typeof this.photographer.netBalance == 'undefined') {
        this.photographer.netBalance = 0;
      }
      this.photographerProvider.updatePhotographer(this.photographer, this.photographer._id)
        .subscribe((response) => {
          if (response.success) {
            this.presentToast('Your profile successfully updated.');
            this.dismiss();
          }
        });
    }
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
