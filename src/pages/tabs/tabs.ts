import { Component } from '@angular/core';
import {HomePage} from "../home/home";
import {PhotoPage} from "../photo/photo";
import {AlbumPage} from "../album/album";
import {CategoryPage} from "../category/category";
import { App, MenuController } from 'ionic-angular';
import {SettingsPage} from "../settings/settings";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = PhotoPage;
  tab3Root = AlbumPage;
  tab4Root = SettingsPage;

  constructor(app: App, menu: MenuController) {
    menu.enable(true);
  }
}
