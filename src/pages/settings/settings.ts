import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {ViewProfilePage} from "../view-profile/view-profile";
import {EditProfilePage} from "../edit-profile/edit-profile";
import {PhotographerAlbumListPage} from "../photographer-album-list/photographer-album-list";
import {AddAlbumPage} from "../add-album/add-album";
import {PhotographerPhotosListPage} from "../photographer-photos-list/photographer-photos-list";
import {AddPhotosPage} from "../add-photos/add-photos";
import {WishlistPage} from "../wishlist/wishlist";
import {AllMessagePage} from "../all-message/all-message";
import {UnseenMessagePage} from "../unseen-message/unseen-message";
import {BuylistPage} from "../buylist/buylist";
import {AddCategoryPage} from "../add-category/add-category";
import {SelectAlbumPage} from "../select-album/select-album";
import {Page} from "ionic-angular/navigation/nav-util";
import {LoginPage} from "../login/login";

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  loginPage: Page = LoginPage;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  openViewProfile() {
    let modal = this.modalCtrl.create(ViewProfilePage);
    modal.present();
  }

  openEditProfile() {
    let modal = this.modalCtrl.create(EditProfilePage);
    modal.present();
  }

  openAlbumListModal() {
    let modal = this.modalCtrl.create(PhotographerAlbumListPage);
    modal.present();
  }

  openAddAlbumModal() {
    let modal = this.modalCtrl.create(AddAlbumPage);
    modal.present();
  }

  openPhotoListModal() {
    let modal = this.modalCtrl.create(PhotographerPhotosListPage);
    modal.present();
  }

  openAddPhotoModal() {
    let modal = this.modalCtrl.create(SelectAlbumPage);
    modal.present();
  }

  openWishListModal() {
    let modal = this.modalCtrl.create(WishlistPage);
    modal.present();
  }

  openBuyListModal() {
    let modal = this.modalCtrl.create(BuylistPage);
    modal.present();
  }

  openAllMessageModal() {
    let modal = this.modalCtrl.create(AllMessagePage);
    modal.present();
  }

  openUnseenMessageModal() {
    let modal = this.modalCtrl.create(UnseenMessagePage);
    modal.present();
  }

  openAddCategoryModal() {
    let modal = this.modalCtrl.create(AddCategoryPage);
    modal.present();
  }

  becomePremium() {

  }

  logout() {
    localStorage.clear();
    this.navCtrl.push(this.loginPage);
  }


}
