import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {PhotographerProvider} from "../../providers/photographer/photographer";
import {User} from "../../model/user";
import {Photographer} from "../../model/photographer";
import {Album} from "../../model/album";
import {Photo} from "../../model/photo";

/**
 * Generated class for the ViewProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-profile',
  templateUrl: 'view-profile.html',
})
export class ViewProfilePage {

  currentUserObj;
  user: User;
  photographer: Photographer;
  albums: Album[];
  photos: Photo[];

  albumsLength: number = 0;
  photosLength: number = 0;
  buyListsLength: number = 0;
  wishListsLength: number = 0;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              private photographerProvider: PhotographerProvider,
              private toastCtrl: ToastController) {
    this.photographer = new Photographer();
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.user = this.currentUserObj.currentUser;
      this.photographerProvider.getPhotographerPrivateInformation(this.user.photographerId)
        .subscribe((response) => {
          if (response.success) {
            this.photographer = response.data.photographer;
            this.albums = response.data.albums;
            this.photos = response.data.photos;
            this.albumsLength = this.albums.length;
            this.photosLength = this.photos.length;
            this.buyListsLength = this.photographer.buyLists.length;
            this.wishListsLength = this.photographer.wishlists.length;
            console.log(this.photographer);
            console.log(this.albums);
            console.log(this.photos);
          } else {

          }
        });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewProfilePage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
