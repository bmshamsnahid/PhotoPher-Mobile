import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhotographerPublicProfilePage } from './photographer-public-profile';

@NgModule({
  declarations: [
    PhotographerPublicProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(PhotographerPublicProfilePage),
  ],
})
export class PhotographerPublicProfilePageModule {}
