import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {Photographer} from "../../model/photographer";

/**
 * Generated class for the PhotographerPublicProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-photographer-public-profile',
  templateUrl: 'photographer-public-profile.html',
})
export class PhotographerPublicProfilePage implements OnInit {

  photographer: Photographer;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PhotographerPublicProfilePage');
  }

  ngOnInit() {
    this.photographer = this.navParams.data;
    console.log(this.photographer);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
