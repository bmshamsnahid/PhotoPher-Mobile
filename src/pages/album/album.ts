import {Component, OnInit} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {SinglePhotoViewerPage} from "../single-photo-viewer/single-photo-viewer";
import {AlbumPhotoPage} from "../album-photo/album-photo";
import {SinglePhotographerProfilePageModule} from "../single-photographer-profile/single-photographer-profile.module";
import {SinglePhotographerProfilePage} from '../single-photographer-profile/single-photographer-profile';
import {AlbumProvider} from "../../providers/album/album";
import {AlbumInfo} from "../../model/albumInfo";
import {Photographer} from "../../model/photographer";

/**
 * Generated class for the AlbumPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-album',
  templateUrl: 'album.html',
})
export class AlbumPage {

  albumsInfo: AlbumInfo[];
  rootAlbumsInfo: AlbumInfo[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              private albumProvider: AlbumProvider) {
  }

  ionViewDidLoad() {
    this.initializeAlbumsInfoList()
  }

  initializeAlbumsInfoList() {
    this.albumProvider.getAllPublicAlbumsInfo()
      .subscribe((response) => {
        if (response.success) {
          this.albumsInfo = response.data;
          this.rootAlbumsInfo = this.albumsInfo;
        }
      });
  }

  getItems(ev) {
    this.albumsInfo = this.rootAlbumsInfo;

    var val = ev.target.value;

    if (val && val.trim() != '') {
      this.albumsInfo = this.albumsInfo.filter((albumInfo: AlbumInfo) => {
        return ((albumInfo.album.name.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
          (albumInfo.owner.name.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
          (albumInfo.owner.contactNumber.toLowerCase().indexOf(val.toLowerCase()) > -1)
        );
      });
    }
  }

  onClickViewAlbum(albumInfo: AlbumInfo) {
    let modal = this.modalCtrl.create(AlbumPhotoPage, albumInfo);
    modal.present();
  }

  onClickViewAlbumOwner(photographer: Photographer) {
    let modal = this.modalCtrl.create(SinglePhotographerProfilePage);
    modal.present();
  }
}
