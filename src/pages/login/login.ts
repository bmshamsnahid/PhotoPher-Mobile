import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {SignupPage} from "../signup/signup";
import {Page} from "ionic-angular/navigation/nav-util";
import { Storage } from '@ionic/storage';
import {User} from "../../model/user";
import {SigninProvider} from "../../providers/signin/signin";
import {HomePage} from "../home/home";
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit {

  homePage: Page = HomePage;
  tabsPage: Page = TabsPage;
  signUpPage: Page = SignupPage;

  user: User;

  userEmail: string;
  userPassword: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private signinProvider: SigninProvider,
              public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  ngOnInit() {
    this.user = new User();
  }

  onClickLogin() {
    console.log('Email: ' + this.userEmail);
    console.log('Password: ' + this.userPassword);

    this.user.email=  this.userEmail;
    this.user.password = this.userPassword;

    this.signinProvider.userSignIn(this.user).subscribe((response) => {
      console.log('Response');
      if (response.success) {
        this.navCtrl.push(this.tabsPage);
        this.presentToast('Successfully logged in.');
      } else {
        this.presentToast('Error in login');
      }
    });
  }

  onClickSignup() {
    this.navCtrl.push(this.signUpPage);
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
