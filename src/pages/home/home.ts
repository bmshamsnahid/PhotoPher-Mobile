import { Component, OnInit } from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams, ToastController} from 'ionic-angular';
import {AlbumPhotoPage} from "../album-photo/album-photo";
import {PhotographerPublicAlbumPage} from "../photographer-public-album/photographer-public-album";
import {PhotographerPublicProfilePage} from "../photographer-public-profile/photographer-public-profile";
import {Photographer} from "../../model/photographer";
import {PhotographerProvider} from "../../providers/photographer/photographer";
import {CallNumber} from '@ionic-native/call-number';
import {SMS} from "@ionic-native/sms";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  photographers: Photographer[];
  rootPhotographers: Photographer[];
  selectedPhotographer: Photographer;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              public alertCtrl: AlertController,
              private photographerProvider: PhotographerProvider,
              private callNumber: CallNumber,
              private sms: SMS,
              public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    this.initializePhotosList();
  }

  initializePhotosList() {
    this.photographerProvider.getAllPhotographers()
      .subscribe((response) => {
        this.photographers = response.data;
        this.rootPhotographers = response.data;
      });
  }

  getItems(ev) {
    this.photographers = this.rootPhotographers;

    var val = ev.target.value;

    if (val && val.trim() != '') {
      this.photographers = this.photographers.filter((photographer) => {
        return ((photographer.name.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
          (photographer.contactNumber.toLowerCase().indexOf(val.toLowerCase()) > -1)
        );
      });
    }
  }

  onClickViewPublicAlbums(photographer: Photographer) {
    let modal = this.modalCtrl.create(PhotographerPublicAlbumPage, photographer);
    modal.present();
  }

  onClickViewPublicProfile(photographer: Photographer) {
    let modal = this.modalCtrl.create(PhotographerPublicProfilePage);
    modal.present();
  }

  callPhotographer() {
    this.callNumber.callNumber("18001010101", true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  messagePhotographer(photographer: Photographer) {
    this.selectedPhotographer = photographer;
    let prompt = this.alertCtrl.create({
      title: 'Message',
      message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'title',
          placeholder: 'Message Title'
        }, {
          name: 'description',
          placeholder: 'Message Description'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            let messageTitle = data.title;
            let messageDescription = data.description;
            console.log('Title: ' + messageTitle);
            console.log('Description: ' + messageDescription);
            if (!messageTitle) {
              this.presentToast('Invalid message title.');
            } else if (!messageDescription) {
              this.presentToast('Invalid message description.')
            } else {
              if (photographer.contactNumber) {
                this.sms.send(photographer.contactNumber, messageDescription);
              } else {
                this.presentToast('Invalid phone number. Unable to message.');
              }
            }
          }
        }
      ]
    });
    prompt.present();
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
