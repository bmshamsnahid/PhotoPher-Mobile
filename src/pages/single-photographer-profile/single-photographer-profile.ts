import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController, AlertController} from 'ionic-angular';

/**
 * Generated class for the SinglePhotographerProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-single-photographer-profile',
  templateUrl: 'single-photographer-profile.html',
})
export class SinglePhotographerProfilePage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SinglePhotographerProfilePage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  callPhotographer() {

  }

  messagePhotographer() {
    let prompt = this.alertCtrl.create({
      title: 'Message',
      message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'title',
          placeholder: 'Message Title'
        }, {
          name: 'description',
          placeholder: 'Message Description'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Message Sent');
          }
        }
      ]
    });
    prompt.present();
  }

}
