import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SinglePhotographerProfilePage } from './single-photographer-profile';

@NgModule({
  declarations: [
    SinglePhotographerProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(SinglePhotographerProfilePage),
  ],
})
export class SinglePhotographerProfilePageModule {}
