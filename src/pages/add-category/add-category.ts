import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {Category} from "../../model/category";
import {CategoryProvider} from "../../providers/category/category";

/**
 * Generated class for the AddCategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-category',
  templateUrl: 'add-category.html',
})
export class AddCategoryPage {

  category: Category;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private categoryProvider: CategoryProvider,
              private toastCtrl: ToastController,
              public viewCtrl: ViewController) {
    this.category = new Category();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddCategoryPage');
  }

  onClickCreateCategory() {
    if (!this.category.title) {
      this.presentToast('Invalid or incomplete category title.');
    } else if (!this.category.description) {
      this.presentToast('Invalid or incomplete category description.');
    } else {
      this.categoryProvider.createCategory(this.category)
        .subscribe((response) => {
          if (response.success) {
            this.presentToast('Category added successfully.');
            this.dismiss();
          }
        });
    }
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
