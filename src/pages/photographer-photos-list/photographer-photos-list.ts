import { Component, OnInit } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {SinglePhotoViewerPage} from "../single-photo-viewer/single-photo-viewer";
import {SinglePhotoEditorPage} from "../single-photo-editor/single-photo-editor";
import {PhotoProvider} from "../../providers/photo/photo";
import {Photo} from "../../model/photo";
import {MyConfig} from "../../model/my-config";

/**
 * Generated class for the PhotographerPhotosListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-photographer-photos-list',
  templateUrl: 'photographer-photos-list.html',
})
export class PhotographerPhotosListPage implements OnInit {

  photos: Photo[];
  myConfig: MyConfig;
  baseUrl: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public modalCtrl: ModalController,
              private photoProvider: PhotoProvider,
              private toastCtrl: ToastController) {
  }

  ngOnInit() {
    this.myConfig = new MyConfig();
    this.baseUrl = this.myConfig.baseURL;
    this.photoProvider.getPhotographerAllPhotos()
      .subscribe((response) => {
        if (response.success) {
          this.photos = response.data;
        }
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PhotographerPhotosListPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onClickViewPhoto(photo: Photo) {
    let modal = this.modalCtrl.create(SinglePhotoViewerPage, photo);
    modal.present();
  }

  onClickDeletePhoto(photo: Photo) {
    this.photoProvider.deletePhoto(photo)
      .subscribe((response) => {
        if (response.success) {
          this.presentToast('Successfully deleted the photo.');
          this.photos = this.runtimePhotoDelete(this.photos, photo);
        }
      });
  }

  runtimePhotoDelete (myPhotos: Photo[], myPhoto: Photo) {
    let newPhotos: Photo[] = [];
    for (let index=0; index<myPhotos.length; index++) {
      let currentPhoto: Photo = myPhotos[index];
      if (currentPhoto._id != myPhoto._id) {
        newPhotos.push(currentPhoto);
      }
    }
    return newPhotos;
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
