import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhotographerPhotosListPage } from './photographer-photos-list';

@NgModule({
  declarations: [
    PhotographerPhotosListPage,
  ],
  imports: [
    IonicPageModule.forChild(PhotographerPhotosListPage),
  ],
})
export class PhotographerPhotosListPageModule {}
