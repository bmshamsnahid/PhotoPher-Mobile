import { Component, OnInit } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {Page} from "ionic-angular/navigation/nav-util";
import {LoginPage} from "../login/login";
import {User} from "../../model/user";
import {SignupProvider} from "../../providers/signup/signup";
import {Photographer} from "../../model/photographer";

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage implements OnInit {

  public loginPage: Page = LoginPage;
  photographer: Photographer;

  username: string;
  email: string;
  password: string;
  retypePassword: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl: ToastController,
              private signUpProvider: SignupProvider) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  ngOnInit() {
    this.photographer = new Photographer();
  }

  onClickLogin() {
    this.navCtrl.push(this.loginPage);
  }

  onClickSignup() {
    if (!this.username) {
      this.presentToast('Invalid username.');
    } else if (!this.email) {
      this.presentToast('Invalid email.');
    } else if (!this.password) {
      this.presentToast('Invalid password');
    } else if (this.password != this.retypePassword) {
      this.presentToast('Password does not match.');
    } else {
      this.photographer.isPremium = false;
      this.photographer.netBalance = 0;
      this.photographer.name = this.username;
      this.photographer.password = this.password;
      this.photographer.email = this.email;

      this.signUpProvider.userSignUp(this.photographer)
        .subscribe((response: any) => {
          console.log(response);
          if (response.success) {
            this.navCtrl.push(LoginPage);
          } else if (response.message) {
            this.presentToast(response.message);
          } else {
            this.presentToast('Fatal server error');
          }
        });
    }
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
}
