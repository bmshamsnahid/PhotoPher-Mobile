import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PublicSinglePhotoViewerPage } from './public-single-photo-viewer';

@NgModule({
  declarations: [
    PublicSinglePhotoViewerPage,
  ],
  imports: [
    IonicPageModule.forChild(PublicSinglePhotoViewerPage),
  ],
})
export class PublicSinglePhotoViewerPageModule {}
