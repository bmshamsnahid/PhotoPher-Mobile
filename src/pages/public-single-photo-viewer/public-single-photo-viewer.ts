import {Component, OnInit} from '@angular/core';
import {
  ActionSheetController,
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  ViewController
} from 'ionic-angular';
import {Photo} from "../../model/photo";
import {MyConfig} from "../../model/my-config";
import {Photographer} from "../../model/photographer";
import {PhotographerProvider} from "../../providers/photographer/photographer";
import {PhotoProvider} from "../../providers/photo/photo";

/**
 * Generated class for the PublicSinglePhotoViewerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-public-single-photo-viewer',
  templateUrl: 'public-single-photo-viewer.html',
})
export class PublicSinglePhotoViewerPage implements OnInit {

  photo: Photo;
  myConfig: MyConfig;
  baseUrl: string;

  currentUserObj;
  photographer: Photographer;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private viewCtrl: ViewController,
              private toastCtrl: ToastController,
              private photoProvider: PhotoProvider,
              private photographerProvider: PhotographerProvider,
              public actionSheetCtrl: ActionSheetController) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    this.photographer = this.currentUserObj.currentUser;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PublicSinglePhotoViewerPage');
  }

  ngOnInit() {
    this.myConfig = new MyConfig();
    this.baseUrl = this.myConfig.baseURL;
    this.photo = this.navParams.data;
    console.log(this.photo);
  }

  photoActionSheet(photo: Photo) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Action On Photo',
      buttons: [
        {
          text: 'Like',
          icon: 'thumbs-up',
          handler: () => {
            photo.likes = photo.likes + 1;
            this.photoProvider.updatePhoto(photo)
              .subscribe((response) => {});
          }
        },{
          text: 'UpVote',
          icon: 'arrow-up',
          handler: () => {
            photo.upVote = photo.upVote + 1;
            this.photoProvider.updatePhoto(photo)
              .subscribe((response) => {});

          }
        },{
          text: 'Downvote',
          icon: 'arrow-down',
          handler: () => {
            photo.upVote = photo.upVote - 1;
            this.photoProvider.updatePhoto(photo)
              .subscribe((response) => {});
          }
        },{
          text: 'Buy',
          icon: 'folder',
          handler: () => {
            console.log('Buy clicked');
          }
        },{
          text: 'Add To Cart',
          icon: 'cart',
          handler: () => {
            let photoAlreadyExists = false;

            if (typeof this.photographer.wishlists == 'undefined') {
              this.photographer.wishlists = [];
              this.photographer.wishlists.push(photo._id);
            } else {
              for (let photoId of this.photographer.wishlists) {
                if (photoId == photo._id) {
                  photoAlreadyExists = true;
                  break;
                }
              }
            }
            if (!photoAlreadyExists) {
              this.photographer.wishlists.push(photo._id);
              this.photographerProvider.updatePhotographer(this.photographer, this.photographer._id)
                .subscribe((response) => {
                  this.presentToast('Photo is added to the cart');
                });
            } else {
              this.presentToast('Photo already exists in your cart');
            }

          }
        },{
          text: 'Report',
          icon: 'undo',
          handler: () => {
            console.log('Report clicked');
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
