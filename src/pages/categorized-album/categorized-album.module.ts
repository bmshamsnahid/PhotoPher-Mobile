import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategorizedAlbumPage } from './categorized-album';

@NgModule({
  declarations: [
    CategorizedAlbumPage,
  ],
  imports: [
    IonicPageModule.forChild(CategorizedAlbumPage),
  ],
})
export class CategorizedAlbumPageModule {}
