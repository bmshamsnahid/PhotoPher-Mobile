import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ViewController} from 'ionic-angular';
import {AlbumPhotoPage} from "../album-photo/album-photo";
import {SinglePhotographerProfilePage} from "../single-photographer-profile/single-photographer-profile";

/**
 * Generated class for the CategorizedAlbumPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categorized-album',
  templateUrl: 'categorized-album.html',
})
export class CategorizedAlbumPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategorizedAlbumPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onClickViewAlbum() {
    let modal = this.modalCtrl.create(AlbumPhotoPage);
    modal.present();
  }

  onClickViewAlbumOwner() {
    let modal = this.modalCtrl.create(SinglePhotographerProfilePage);
    modal.present();
  }

}
