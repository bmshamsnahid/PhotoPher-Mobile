import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {SinglePhotoViewerPage} from "../single-photo-viewer/single-photo-viewer";
import {Photographer} from "../../model/photographer";
import {User} from "../../model/user";
import {MyConfig} from "../../model/my-config";
import {Photo} from "../../model/photo";
import {AlbumInfo} from "../../model/albumInfo";
import {PhotographerProvider} from "../../providers/photographer/photographer";
import {WishlistsProvider} from "../../providers/wishlists/wishlists";
import {PublicSinglePhotoViewerPage} from "../public-single-photo-viewer/public-single-photo-viewer";

/**
 * Generated class for the WishlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-wishlist',
  templateUrl: 'wishlist.html',
})
export class WishlistPage {
  myConfig: MyConfig;

  albumInfo: AlbumInfo;
  photos: Photo[];

  currentUserObj;
  photographer: Photographer;
  baseUrl: string;
  user: User;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public modalCtrl: ModalController,
              private photographerProvider: PhotographerProvider,
              private toastCtrl: ToastController,
              private wishListProvider: WishlistsProvider) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    this.user = this.currentUserObj.currentUser;
    this.myConfig = new MyConfig();
    this.baseUrl = this.myConfig.baseURL;
    this.wishListProvider.getWishListsPhoto()
      .subscribe((response) => {
        if (response.success) {
          this.photos = response.data;
        }
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WishlistPage');
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onClickViewPhoto(photo: Photo) {
    let modal = this.modalCtrl.create(PublicSinglePhotoViewerPage, photo);
    modal.present();
  }

  onClickBuyPhoto(photo: Photo) {

  }

  onClickRemovePhoto(photo: Photo) {
    this.wishListProvider.removePhotoFromWishLists(photo)
      .subscribe((response) => {
        if (response.success) {
          this.presentToast('Successfully deleted the photo.');
          this.photos = this.runtimePhotoDelete(this.photos, photo);
        }
      });
  }

  runtimePhotoDelete (myPhotos: Photo[], myPhoto: Photo) {
    let newPhotos: Photo[] = [];
    for (let index=0; index<myPhotos.length; index++) {
      let currentPhoto: Photo = myPhotos[index];
      if (currentPhoto._id != myPhoto._id) {
        newPhotos.push(currentPhoto);
      }
    }
    return newPhotos;
  }

}
