import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SinglePhotoViewerPage } from './single-photo-viewer';

@NgModule({
  declarations: [
    SinglePhotoViewerPage,
  ],
  imports: [
    IonicPageModule.forChild(SinglePhotoViewerPage),
  ],
})
export class SinglePhotoViewerPageModule {}
