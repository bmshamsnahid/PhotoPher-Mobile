import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {Photo} from "../../model/photo";
import {MyConfig} from "../../model/my-config";
import {AlbumProvider} from "../../providers/album/album";
import {PhotoProvider} from "../../providers/photo/photo";
import {CategoryProvider} from "../../providers/category/category";
import {Category} from "../../model/category";
import {Album} from "../../model/album";

/**
 * Generated class for the SinglePhotoViewerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-single-photo-viewer',
  templateUrl: 'single-photo-viewer.html',
})
export class SinglePhotoViewerPage implements OnInit {

  myConfig: MyConfig;
  photo: Photo;
  baseUrl: string;
  categories: Category[];
  albums: Album[];
  selectedAlbumId: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              private albumProvider: AlbumProvider,
              private photoProvider: PhotoProvider,
              private categoryProvider: CategoryProvider,
              private toastCtrl: ToastController) {
    this.myConfig = new MyConfig();
  }

  ngOnInit() {
    this.photo = this.navParams.data;
    this.baseUrl = this.myConfig.baseURL;
    this.albumProvider.getPhotographerAllAlbums()
      .subscribe((response) => {
        if (response.success) {
          this.albums = response.data;
        }
      });
  }

  onClickUpdatePhoto() {
    if (!this.photo.displayName) {
      this.presentToast('Invalid or incomplete photo display name.');
    } else {
      if (typeof this.selectedAlbumId != 'undefined') {
        this.photo.albumId = this.selectedAlbumId;
      }
      this.photoProvider.updatePhoto(this.photo)
        .subscribe((response) => {
          if (response.success) {
            this.presentToast('Successfully updated the photo');
            this.dismiss();
          }
        });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SinglePhotoViewerPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
