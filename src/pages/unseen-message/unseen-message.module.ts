import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UnseenMessagePage } from './unseen-message';

@NgModule({
  declarations: [
    UnseenMessagePage,
  ],
  imports: [
    IonicPageModule.forChild(UnseenMessagePage),
  ],
})
export class UnseenMessagePageModule {}
