import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {Album} from "../../model/album";
import {Category} from "../../model/category";
import {CategoryProvider} from "../../providers/category/category";
import {AlbumProvider} from "../../providers/album/album";
import {User} from "../../model/user";

@IonicPage()
@Component({
  selector: 'page-add-album',
  templateUrl: 'add-album.html',
})
export class AddAlbumPage {

  categories: Category[];
  selectedCategory: Category;
  isPrivate: any;
  album: Album;

  currentUserObj;
  user: User;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              private categoryProvider: CategoryProvider,
              private albumProvider: AlbumProvider,
              public toastCtrl: ToastController) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj != null) {
      this.user = this.currentUserObj.currentUser;
    }

    this.album = new Album();
    this.categoryProvider.getAllCategory()
      .subscribe((response) => {
        if (response.success) {
          this.categories = response.data;
        }
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddAlbumPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onClickCreateAlbum() {
    console.log(this.album);
    if (!this.album.name) {
      this.presentToast('Invalid or incomplete album name.');
    } else if (!this.album.description) {
      this.presentToast('Invalid or incomplete album description.');
    } else if (!this.album.category) {
        this.presentToast('Invalid or incomplete album category.');
    } else {
      if (typeof this.album.isPrivate == 'undefined') {
        this.album.isPrivate = false;
      }
      this.album.ownerId = this.user.photographerId;
      this.albumProvider.createAlbum(this.album)
        .subscribe((response) => {
          if (response.success) {
            this.presentToast('Successfully created the album');
            this.viewCtrl.dismiss();
          }
        });
    }
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
