import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {FileUploader} from "ng2-file-upload";
import {MyConfig} from "../../model/my-config";
import {AlbumProvider} from "../../providers/album/album";
import {Album} from '../../model/album';

/**
 * Generated class for the AddPhotosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-photos',
  templateUrl: 'add-photos.html',
})
export class AddPhotosPage {

  myConfig: MyConfig = new MyConfig();
  uploader: FileUploader;
  selectedAlbumId: string;
  attachmentList: any = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              private albumProvider: AlbumProvider,
              private toastCtrl: ToastController) {
    this.selectedAlbumId = this.navParams.data.id;
    this.uploader = new FileUploader({url: `${this.myConfig.baseURL}/api/photo/upload/${this.selectedAlbumId}`});
    this.uploader.onAfterAddingFile = (file)=> { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item:any, response:any , status:any, headers:any) => {
      this.attachmentList.push(JSON.parse(response));
      console.log(JSON.parse(response));
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPhotosPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
