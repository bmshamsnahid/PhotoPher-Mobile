import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPhotosPage } from './add-photos';
import {FileUploadModule} from "ng2-file-upload";

@NgModule({
  declarations: [
    AddPhotosPage,

  ],
  imports: [
    FileUploadModule,
    IonicPageModule.forChild(AddPhotosPage),
  ],
})
export class AddPhotosPageModule {}
