import { Component, OnInit } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ViewController} from 'ionic-angular';
import {SinglePhotographerProfilePage} from "../single-photographer-profile/single-photographer-profile";
import {AlbumPhotoPage} from "../album-photo/album-photo";
import {Photographer} from "../../model/photographer";
import {Album} from "../../model/album";
import {AlbumProvider} from "../../providers/album/album";
import {AlbumInfo} from "../../model/albumInfo";

/**
 * Generated class for the PhotographerPublicAlbumPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-photographer-public-album',
  templateUrl: 'photographer-public-album.html',
})
export class PhotographerPublicAlbumPage implements OnInit {

  photographer: Photographer;
  albumInfos: AlbumInfo[];
  title: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public modalCtrl: ModalController,
              private albumProvider: AlbumProvider) {
  }

  ionViewDidLoad() {
    console.log('Photographer Public album page.');
  }

  ngOnInit() {
    this.photographer = this.navParams.data;
    this.title = this.photographer.name + '\'s  Albums';
    this.albumProvider.getPhotographerPublicAlbums(this.photographer._id)
      .subscribe((response) => {
        this.albumInfos = response.data;
        console.log(this.albumInfos);
      });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onClickViewAlbum(albumInfo: AlbumInfo) {
    let modal = this.modalCtrl.create(AlbumPhotoPage, albumInfo);
    modal.present();
  }

  onClickViewAlbumOwner() {
    let modal = this.modalCtrl.create(SinglePhotographerProfilePage);
    modal.present();
  }

}
