import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhotographerPublicAlbumPage } from './photographer-public-album';

@NgModule({
  declarations: [
    PhotographerPublicAlbumPage,
  ],
  imports: [
    IonicPageModule.forChild(PhotographerPublicAlbumPage),
  ],
})
export class PhotographerPublicAlbumPageModule {}
