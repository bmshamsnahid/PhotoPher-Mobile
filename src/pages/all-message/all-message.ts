import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the AllMessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-all-message',
  templateUrl: 'all-message.html',
})
export class AllMessagePage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AllMessagePage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'Contact',
      message: "To send a message, enter the text below.",
      inputs: [
        {
          name: 'message',
          placeholder: 'Message'
        },
      ],
      buttons: [
        {
          text: 'Message',
          handler: data => {
            console.log('Message Clicked');
          }
        },
        {
          text: 'Call',
          handler: data => {
            console.log('Making Call');
          }
        }
      ]
    });
    prompt.present();
  }

}
