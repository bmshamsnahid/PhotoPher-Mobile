import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllMessagePage } from './all-message';

@NgModule({
  declarations: [
    AllMessagePage,
  ],
  imports: [
    IonicPageModule.forChild(AllMessagePage),
  ],
})
export class AllMessagePageModule {}
