import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ViewController} from 'ionic-angular';
import {SinglePhotoViewerPage} from "../single-photo-viewer/single-photo-viewer";

/**
 * Generated class for the BuylistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-buylist',
  templateUrl: 'buylist.html',
})
export class BuylistPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BuylistPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onClickViewPhoto() {
    let modal = this.modalCtrl.create(SinglePhotoViewerPage);
    modal.present();
  }

  onClickRemovePhoto() {

  }

}
