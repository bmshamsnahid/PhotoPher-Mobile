import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuylistPage } from './buylist';

@NgModule({
  declarations: [
    BuylistPage,
  ],
  imports: [
    IonicPageModule.forChild(BuylistPage),
  ],
})
export class BuylistPageModule {}
