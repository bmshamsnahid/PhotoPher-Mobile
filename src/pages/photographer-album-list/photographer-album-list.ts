import {Component, OnInit} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {EditAlbumPage} from "../edit-album/edit-album";
import {AlbumProvider} from "../../providers/album/album";
import {Album} from "../../model/album";
import {AlbumInfo} from "../../model/albumInfo";

/**
 * Generated class for the PhotographerAlbumListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-photographer-album-list',
  templateUrl: 'photographer-album-list.html',
})
export class PhotographerAlbumListPage implements OnInit {

  albums: Album[];
  albumsInfo: AlbumInfo[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public modalCtrl: ModalController,
              private albumProvider: AlbumProvider,
              public toastCtrl: ToastController) {
    console.log('In the Album List Page');
    this.albumProvider.getAllPublicAndPrivateAlbumsInfo()
      .subscribe((response) => {
        console.log('Response');
        console.log(response);
        if (response.success) {
          this.albumsInfo = response.data;
          console.log(this.albumsInfo);
        }
      });
  }

  ngOnInit() {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PhotographerAlbumListPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onClickAlbumEdit(albumInfo: AlbumInfo) {
    let modal = this.modalCtrl.create(EditAlbumPage, albumInfo);
    modal.present();
  }

  onClickDeleteAlbum(album: Album) {
    this.presentToast('Instead of delete your album is now private mode, No one can see your album.');
    album.isPrivate = true;
    this.albumProvider.updateAlbum(album)
      .subscribe((response) => {
        this.presentToast('Now your album: ' + album.name + ' is private');
      });
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
