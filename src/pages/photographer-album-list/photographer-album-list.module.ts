import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhotographerAlbumListPage } from './photographer-album-list';

@NgModule({
  declarations: [
    PhotographerAlbumListPage,
  ],
  imports: [
    IonicPageModule.forChild(PhotographerAlbumListPage),
  ],
})
export class PhotographerAlbumListPageModule {}
