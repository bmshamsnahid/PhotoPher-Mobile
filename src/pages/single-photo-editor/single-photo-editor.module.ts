import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SinglePhotoEditorPage } from './single-photo-editor';

@NgModule({
  declarations: [
    SinglePhotoEditorPage,
  ],
  imports: [
    IonicPageModule.forChild(SinglePhotoEditorPage),
  ],
})
export class SinglePhotoEditorPageModule {}
