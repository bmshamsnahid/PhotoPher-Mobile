import { Component, OnInit } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {ViewProfilePage} from "../view-profile/view-profile";
import {SinglePhotoViewerPage} from "../single-photo-viewer/single-photo-viewer";
import {SinglePhotoEditorPage} from "../single-photo-editor/single-photo-editor";
import {Album} from "../../model/album";
import {CategoryProvider} from "../../providers/category/category";
import {Category} from "../../model/category";
import {AlbumProvider} from "../../providers/album/album";
import {AlbumInfo} from "../../model/albumInfo";
import {Photo} from "../../model/photo";
import {MyConfig} from "../../model/my-config";
import {PhotoProvider} from "../../providers/photo/photo";

@IonicPage()
@Component({
  selector: 'page-edit-album',
  templateUrl: 'edit-album.html',
})
export class EditAlbumPage implements OnInit {

  myConfig: MyConfig = new MyConfig();
  isAlbumPrivate: boolean;
  selectedCategoryId: string;
  album: Album;
  albumInfo: AlbumInfo;
  photos: Photo[];
  categories: Category[];
  baseUrl: string = this.myConfig.baseURL;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public modalCtrl: ModalController,
              private categoryProvider: CategoryProvider,
              private albumProvider: AlbumProvider,
              public toastCtrl: ToastController,
              private photoProvider: PhotoProvider) {
    this.albumInfo = this.navParams.data;
    this.album = this.albumInfo.album;
    this.photos = this.albumInfo.photos;
    this.categoryProvider.getAllCategory()
      .subscribe((response) => {
        if (response.success) {
          this.categories = response.data;
        }
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditAlbumPage');
  }

  ngOnInit() {
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onClickViewPhoto(photo: Photo) {
    let modal = this.modalCtrl.create(SinglePhotoViewerPage, photo);
    modal.present();
  }

  onClickDeletePhoto(photo: Photo) {
    this.photoProvider.deletePhoto(photo)
      .subscribe((response) => {
        if (response.success) {
          this.presentToast('Successfully deleted the photo.');
          this.photos = this.runtimePhotoDelete(this.photos, photo);
        }
      });
  }

  onClickUpdateAlbum() {
    if (!this.album.name) {
      this.presentToast('Please enter a valid album name.');
    } else if (!this.album.description) {
      this.presentToast('Please enter a valid album description.')
    } else {
      if (typeof this.selectedCategoryId != 'undefined') {
        this.album.category = this.selectedCategoryId;
      }
      this.albumProvider.updateAlbum(this.album)
        .subscribe((response) => {
          if (response.success) {
            this.presentToast('Album Successfully updated.');
          }
        });
    }
  }

  runtimePhotoDelete (myPhotos: Photo[], myPhoto: Photo) {
    let newPhotos: Photo[] = [];
    for (let index=0; index<myPhotos.length; index++) {
      let currentPhoto: Photo = myPhotos[index];
      if (currentPhoto._id != myPhoto._id) {
        newPhotos.push(currentPhoto);
      }
    }
    return newPhotos;
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
