import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {AlbumProvider} from "../../providers/album/album";
import {Album} from "../../model/album";
import {AddPhotosPage} from "../add-photos/add-photos";

/**
 * Generated class for the SelectAlbumPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-album',
  templateUrl: 'select-album.html',
})
export class SelectAlbumPage {

  albums: Album[];
  selectedAlbumId: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private viewCtrl: ViewController,
              private modalCtrl: ModalController,
              private albumProvider: AlbumProvider,
              private toastCtrl: ToastController) {

  }

  ionViewDidLoad() {
    this.albumProvider.getPhotographerAllAlbums()
      .subscribe((response) => {
        if (response.success) {
          this.albums = response.data;
          console.log('Album lists: ');
          console.log(this.albums);
        } else {
          this.presentToast('Error in getting the albums.');
        }
      });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onClickSelectAlbum() {
    if (!this.selectedAlbumId) {
      this.presentToast('Please select a album.');
    } else {
      let modal = this.modalCtrl.create(AddPhotosPage, { id: this.selectedAlbumId });
      modal.present();
    }
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
