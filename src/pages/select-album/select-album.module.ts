import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectAlbumPage } from './select-album';

@NgModule({
  declarations: [
    SelectAlbumPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectAlbumPage),
  ],
})
export class SelectAlbumPageModule {}
