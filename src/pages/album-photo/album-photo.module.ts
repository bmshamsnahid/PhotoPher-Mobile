import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AlbumPhotoPage } from './album-photo';
import {Stripe} from "@ionic-native/stripe";

@NgModule({
  declarations: [
    AlbumPhotoPage
  ],
  imports: [
    IonicPageModule.forChild(AlbumPhotoPage),
  ],
})
export class AlbumPhotoPageModule {}
