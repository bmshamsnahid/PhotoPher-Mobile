import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  ActionSheetController, AlertController,
  IonicPage,
  ModalController,
  NavController,
  NavParams, ToastController,
  ViewController
} from 'ionic-angular';
import {SinglePhotoViewerPage} from "../single-photo-viewer/single-photo-viewer";
import {AlbumInfo} from "../../model/albumInfo";
import {Photo} from "../../model/photo";
import {PhotoProvider} from "../../providers/photo/photo";
import {Photographer} from "../../model/photographer";
import {PhotographerProvider} from "../../providers/photographer/photographer";
import {MyConfig} from "../../model/my-config";
import {PublicSinglePhotoViewerPage} from "../public-single-photo-viewer/public-single-photo-viewer";
import {StripeProvider} from "../../providers/stripe/stripe";
import { NgForm } from '@angular/forms';
import {User} from "../../model/user";
// import {Stripe} from "@ionic-native/stripe";

/**
 * Generated class for the AlbumPhotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-album-photo',
  templateUrl: 'album-photo.html',
})
export class AlbumPhotoPage implements OnInit {

  myConfig: MyConfig;

  albumInfo: AlbumInfo;
  photos: Photo[];

  currentUserObj;
  photographer: Photographer;
  user: User;
  baseUrl: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public actionSheetCtrl: ActionSheetController,
              public modalCtrl: ModalController,
              private photoProvider: PhotoProvider,
              private photographerProvider: PhotographerProvider,
              private toastCtrl: ToastController,
              private cd: ChangeDetectorRef,
              private stripeService: StripeProvider,
              // private stripe: Stripe,
              private alertCtrl: AlertController) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    this.user = this.currentUserObj.currentUser;
    this.photographerProvider.getAPhotographer(this.user.photographerId)
      .subscribe((response) => {
        if (response.success) {
          this.photographer = response.data;
        }
      });
    // this.stripe.setPublishableKey('pk_test_WrUREODJMeeD71YVWPChhOOM');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AlbumPhotoPage');
  }

  ngOnInit() {
    this.myConfig = new MyConfig();
    this.baseUrl = this.myConfig.baseURL;
    this.albumInfo = this.navParams.data;
    this.photos = this.albumInfo.photos;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  photoActionSheet(photo: Photo) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Action On Photo',
      buttons: [
        {
          text: 'View',
          icon: 'eye',
          handler: () => {
            photo.views = photo.views + 1;
            this.photoProvider.updatePhoto(photo)
              .subscribe((response) => {
                let modal = this.modalCtrl.create(PublicSinglePhotoViewerPage, photo);
                modal.present();
              });
          }
        },{
          text: 'Like',
          icon: 'thumbs-up',
          handler: () => {
            photo.likes = photo.likes + 1;
            this.photoProvider.updatePhoto(photo)
              .subscribe((response) => {});
          }
        },{
          text: 'UpVote',
          icon: 'arrow-up',
          handler: () => {
            if (photo.upVoterList.indexOf(this.photographer._id) >= 0) {
              this.presentToast('You already voted for this photo.');
              return;
            } else {
              photo.upVote = photo.upVote + 1;
              photo.upVoterList.push(this.photographer._id);
              this.photoProvider.updatePhoto(photo)
                .subscribe((response) => {
                  this.presentToast('Your vote being casted. Thank you.')
                });
            }
          }
        },{
          text: 'Downvote',
          icon: 'arrow-down',
          handler: () => {
            if (photo.upVoterList.indexOf(this.photographer._id) >= 0) {
              this.presentToast('You already voted for this photo.');
              return;
            } else {
              photo.upVote = photo.upVote - 1;
              photo.upVoterList.push(this.photographer._id);
              this.photoProvider.updatePhoto(photo)
                .subscribe((response) => {
                  this.presentToast('Your vote being casted. Thank you.')
                });
            }
          }
        },{
          text: 'Buy',
          icon: 'folder',
          handler: () => {
            console.log('Buy clicked');
            this.showPrompt();
          }
        },{
          text: 'Add To Cart',
          icon: 'cart',
          handler: () => {
            console.log('Photographer Info');
            console.log(this.photographer);
            if (this.photographer.wishlists.indexOf(photo._id) >= 0) {
              this.presentToast('Photo already exists');
            } else {
              this.photographer.wishlists.push(photo._id);
              this.photographerProvider.updatePhotographer(this.photographer, this.photographer._id).subscribe((response) => {
                console.log(response);
                if (response.success) {
                  this.presentToast('Successfully added to the cart.');
                }
              });
            }
          }
        },{
          text: 'Report',
          icon: 'undo',
          handler: () => {
            console.log('Report clicked');
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'Card Information',
      message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'Number',
          placeholder: 'Card Number'
        },
        {
          name: 'expMonth',
          placeholder: 'Exp. Month'
        },
        {
          name: 'expYear',
          placeholder: 'Exp. Year'
        },
        {
          name: 'CVC',
          placeholder: 'CVC'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Buy',
          handler: data => {
            console.log('Saved clicked');
            let card = {
              number: '4242424242424242',
              expMonth: 12,
              expYear: 2020,
              cvc: '220'
            };
            /*this.stripe.createCardToken(card)
              .then((token) => {
                console.log(token.id);
                this.stripeService.confirmPayment('test@mymail.com', token)
                  .subscribe((response) => {
                    if (response._body) {
                      this.presentToast('Payment success');
                    } else {
                      this.presentToast('Error in payment');
                    }
                  });
              })
              .catch(error => console.error(error));*/
          }
        }
      ]
    });
    prompt.present();
  }

}
