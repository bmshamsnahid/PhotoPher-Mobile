import {Component, OnInit} from '@angular/core';
import {
  ActionSheetController, AlertController,
  IonicPage,
  ModalController,
  NavController,
  NavParams,
  ToastController
} from 'ionic-angular';
import {SinglePhotoViewerPage} from "../single-photo-viewer/single-photo-viewer";
import {PhotoProvider} from "../../providers/photo/photo";
import {Photo} from "../../model/photo";
import {MyConfig} from "../../model/my-config";
import {AlbumInfo} from "../../model/albumInfo";
import {Photographer} from "../../model/photographer";
import {PublicSinglePhotoViewerPage} from "../public-single-photo-viewer/public-single-photo-viewer";
import {PhotographerProvider} from "../../providers/photographer/photographer";
import {User} from "../../model/user";

/**
 * Generated class for the PhotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-photo',
  templateUrl: 'photo.html',
})
export class PhotoPage {

  myConfig: MyConfig;

  albumInfo: AlbumInfo;
  photos: Photo[];

  rootPhotos: Photo[];

  currentUserObj;
  photographer: Photographer;
  baseUrl: string;
  user: User;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public actionSheetCtrl: ActionSheetController,
              public modalCtrl: ModalController,
              private photoProvider: PhotoProvider,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController,
              private photographerProvider: PhotographerProvider) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    this.user = this.currentUserObj.currentUser;
    this.photographerProvider.getAPhotographer(this.user.photographerId)
      .subscribe((response) => {
        if (response.success) {
          this.photographer = response.data;
        }
      });
  }

  ionViewDidLoad() {
    this.myConfig = new MyConfig();
    this.baseUrl = this.myConfig.baseURL;
    this.initializePhotosList();
  }

  initializePhotosList() {
    this.photoProvider.getAllPublicPhotos()
      .subscribe((response) => {
        if (response.success) {
          this.photos = response.data;
          this.rootPhotos = this.photos;
        }
      });
  }

  getItems(ev) {
    this.photos = this.rootPhotos;

    var val = ev.target.value;

    if (val && val.trim() != '') {
      this.photos = this.photos.filter((photo) => {
        return ((photo.displayName.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
          (photo.name.toLowerCase().indexOf(val.toLowerCase()) > -1)
        );
      });
    }
  }

  photoActionSheet(photo: Photo) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Action On Photo',
      buttons: [
        {
          text: 'View',
          icon: 'eye',
          handler: () => {
            photo.views = photo.views + 1;
            this.photoProvider.updatePhoto(photo)
              .subscribe((response) => {
                let modal = this.modalCtrl.create(PublicSinglePhotoViewerPage, photo);
                modal.present();
              });
          }
        },{
          text: 'Like',
          icon: 'thumbs-up',
          handler: () => {
            photo.likes = photo.likes + 1;
            this.photoProvider.updatePhoto(photo)
              .subscribe((response) => {});
          }
        },{
          text: 'UpVote',
          icon: 'arrow-up',
          handler: () => {
            if (photo.upVoterList.indexOf(this.photographer._id) >= 0) {
              this.presentToast('You already voted for this photo.');
              return;
            } else {
              photo.upVote = photo.upVote + 1;
              photo.upVoterList.push(this.photographer._id);
              this.photoProvider.updatePhoto(photo)
                .subscribe((response) => {
                  this.presentToast('Your vote being casted. Thank you.')
                });
            }
          }
        },{
          text: 'Downvote',
          icon: 'arrow-down',
          handler: () => {
            if (photo.upVoterList.indexOf(this.photographer._id) >= 0) {
              this.presentToast('You already voted for this photo.');
              return;
            } else {
              photo.upVote = photo.upVote - 1;
              photo.upVoterList.push(this.photographer._id);
              this.photoProvider.updatePhoto(photo)
                .subscribe((response) => {
                  this.presentToast('Your vote being casted. Thank you.')
                });
            }
          }
        },{
          text: 'Buy',
          icon: 'folder',
          handler: () => {
            console.log('Buy clicked');
            this.showPrompt();
          }
        },{
          text: 'Add To Cart',
          icon: 'cart',
          handler: () => {
            if (typeof this.photographer.wishlists == 'undefined') {
              this.photographer.wishlists = [];
            }
            if (this.photographer.wishlists.indexOf(photo._id) >= 0) {
              this.presentToast('Photo already exists');
            } else {
              this.photographer.wishlists.push(photo._id);
              this.photographerProvider.updatePhotographer(this.photographer, this.photographer._id).subscribe((response) => {
                console.log(response);
                if (response.success) {
                  this.presentToast('Successfully added to the cart.');
                }
              });
            }
          }
        },{
          text: 'Report',
          icon: 'undo',
          handler: () => {
            console.log('Report clicked');
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'Card Information',
      message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'Number',
          placeholder: 'Card Number'
        },
        {
          name: 'expMonth',
          placeholder: 'Exp. Month'
        },
        {
          name: 'expYear',
          placeholder: 'Exp. Year'
        },
        {
          name: 'CVC',
          placeholder: 'CVC'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Buy',
          handler: data => {
            console.log('Saved clicked');
            let card = {
              number: '4242424242424242',
              expMonth: 12,
              expYear: 2020,
              cvc: '220'
            };
            /*this.stripe.createCardToken(card)
              .then((token) => {
                console.log(token.id);
                this.stripeService.confirmPayment('test@mymail.com', token)
                  .subscribe((response) => {
                    if (response._body) {
                      this.presentToast('Payment success');
                    } else {
                      this.presentToast('Error in payment');
                    }
                  });
              })
              .catch(error => console.error(error));*/
          }
        }
      ]
    });
    prompt.present();
  }

}
